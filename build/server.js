"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var email_1 = __importDefault(require("./routes/email"));
var environments_1 = __importDefault(require("./config/environments"));
if (process.env.NODE_ENV !== 'production') {
    var envs = environments_1.default;
    console.log(envs);
}
var app = express_1.default();
var port = process.env.PORT || 3400;
app.use(cors_1.default());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(body_parser_1.default.json());
app.use('/', email_1.default);
app.listen(port, function () { return console.log("Listening on port " + port + ": http://localhost:" + port + "/email/:to/:msg/:txt"); });
