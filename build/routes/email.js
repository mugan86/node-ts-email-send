"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var email_1 = require("../email");
var constants_1 = require("./../config/constants");
var emailRouter = express_1.Router();
emailRouter.get('/email/:to/:msg/:txt', function (req, res) {
    var mailOptions = {
        from: constants_1.FROM,
        to: req.params.to,
        subject: req.params.msg || 'Sending Email using Node.js',
        text: req.params.txt || 'Ya recibo el email'
    };
    email_1.sendEmail(mailOptions).then(function (result) {
        console.log('Resultado satisfactorio: ', result);
        res.json({
            status: true,
            message: 'Email enviado correctamente a '.concat(mailOptions.to),
            from: constants_1.FROM
        });
    }).catch(function (error) {
        console.log('Error inesperado: ', error);
        res.json({
            status: false,
            message: 'Email NO ENVIADO a '.concat(mailOptions.to),
            from: constants_1.FROM
        });
    });
});
exports.default = emailRouter;
