# Node Typescript Enviar Correos electrónicos

Para enviar emails mediante Node con Typescript, mediante la autenticación del usuario en Gmail.

## Instrucciones de uso

1.- Tenemos que clonar el repositorio:
```
git clone https://gitlab.com/mugan86/node-ts-email-send.git
```

2.- Instalamos las dependencias del proyecto:
```
npm install
```
3.- Tenemos que crear el fichero de las variables de entorno con los datos de GMAIL
El fichero que tenemos que crear es **.env** y vamos a añadir el contenido que tenemos en el fichero **".env_example"** dentro del directorio **"src"**.
El contenido es el siguiente:
```
GMAIL_USER='CORREO_GMAIL'
GMAIL_PASS='PASSWORD_GMAIL'
```
4.- Completados estos pasos, únicamente nos queda poner en marcha el servidor en desarrollo:
```
npm run start:dev
```
5.- Añadimos la información en las opciones del email con los datos que queremos 
```
var mailOptions = {
from: 'NOMBRE_REMITENTE <micorreo@gmail.com>', // REMITENTE
to: 'otrocorreo@gmail.com', // EL QUE RECIBE EL CORREO
subject: 'Sending Email using Node.js', //ASUNTO
text: 'Ya recibo el email' //MENSAJE (PUEDE CONTENTER HTML)
}
```
6.- Ejecutamos la URL:
```
http://localhost:3400/email
```
7.- Comprobamos que hemos recibido el correo electrónico correctamente:

![Result](./screens/result.png)