import { Router, Request, Response} from 'express';
import { sendEmail } from '../email';
import { FROM } from './../config/constants';
const emailRouter = Router();

emailRouter.get('/email/:to/:msg/:txt', (req: Request, res: Response) => {
    var mailOptions = {
        from: FROM, // REMITENTE
        to:  req.params.to, // EL QUE RECIBE EL CORREO
        subject: req.params.msg || 'Sending Email using Node.js', //ASUNTO
        text: req.params.txt || 'Ya recibo el email' //MENSAJE (PUEDE CONTENTER HTML)
    };
    sendEmail(mailOptions).then( (result: any) => {
        console.log('Resultado satisfactorio: ', result);
        // res.send('Email enviado correctamente a '.concat(mailOptions.to))
        res.json({
            status: true,
            message: 'Email enviado correctamente a '.concat(mailOptions.to),
            from: FROM
        })
    }).catch(error => {
        console.log('Error inesperado: ', error); 
        // res.send('Email NO ENVIADO a '.concat(mailOptions.to))
        res.json({
            status: false,
            message: 'Email NO ENVIADO a '.concat(mailOptions.to),
            from: FROM
        })
    });
});


export default emailRouter;