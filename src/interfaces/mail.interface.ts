import { File } from './file.interface';
export interface Mail {
    from: string;
    to: string | undefined;
    subject: string;
    text: string;
    attachments?: File
};
