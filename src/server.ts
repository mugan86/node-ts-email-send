import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import emailRouter from './routes/email';
import environments from './config/environments';
if (process.env.NODE_ENV !== 'production') {
    const envs = environments;
    console.log(envs);
}

const app = express();
const port = process.env.PORT || 3400;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', emailRouter);

app.listen(port, () => console.log(`Listening on port ${port}: http://localhost:${port}/email/:to/:msg/:txt`));