import nodemailer from 'nodemailer';
import { Mail } from './interfaces/mail.interface';
export async function sendEmail ( mail: Mail)
{
    return new Promise( async ( resolve, reject ) =>
    {
        console.log("Creating transport...");
        const transporter = await createTransport();
        transporter.sendMail(
            { to: mail.to, from: mail.from, html: mail.text, subject: mail.subject },
            ( error: any, info: any ) => error ? reject( error ) : resolve( info )
        );
    });
}

async function createTransport() {
    return nodemailer.createTransport({
        service: 'gmail', //al usar un servicio bien conocido, no es necesario proveer un nombre de servidor.
        auth: {
            user: process.env.GMAIL_USER, pass: process.env.GMAIL_PASS
        }
    });
}